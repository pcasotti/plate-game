use std::sync::Arc;

use crate::{Renderable, Camera, Vert};

#[derive(Copy, Clone)]
pub struct Transform {
    pub pos: glam::Vec3,
    pub rot: glam::Quat,
}

impl Transform {
    pub const IDENTITY: Self = Self {
        pos: glam::Vec3::ZERO,
        rot: glam::Quat::IDENTITY,
    };

    pub fn new(pos: glam::Vec3, rot: glam::Quat) -> Self {
        Self {
            pos,
            rot,
        }
    }
}

pub trait HasTransform {
    fn transform(&self) -> &Transform;
    fn transform_mut(&mut self) -> &mut Transform;

    fn translation(&self) -> glam::Vec3 {
        self.transform().pos
    }

    fn quat(&self) -> glam::Quat {
        self.transform().rot
    }

    fn mat4(&self) -> glam::Mat4 {
        glam::Mat4::from_rotation_translation(self.quat(), self.translation())
    }

    fn translate(&mut self, dir: glam::Vec3) {
        self.transform_mut().pos += dir;
    }
}

impl<T: HasTransform> Camera for T {
    fn proj(&self, aspect_ration: f32) -> glam::Mat4 {
        glam::Mat4::perspective_lh(45f32.to_radians(), aspect_ration, 0.1, 100.0)
    }
    fn view(&self) -> glam::Mat4 {
        glam::Mat4::from_rotation_translation(self.quat(), self.translation()).inverse()
    }
}

pub struct Model<V> {
    vert_buffer: plate::VertexBuffer<V>,
    index_buffer: plate::IndexBuffer<u32>,
    index_count: u32,
}

impl Model<Vert> {
    pub fn new(device: &Arc<plate::Device>, cmd_pool: &plate::CommandPool) -> Result<Self, Box<dyn std::error::Error>> {
        let mut vertices = vec![];

        let (models, _) = tobj::load_obj(
            "res/untitled.obj",
            &tobj::LoadOptions {
                single_index: true,
                triangulate: true,
                ..Default::default()
            },
        )?;

        let mesh = &models[0].mesh;
        let vertex_count = mesh.positions.len()/3;

        let positions = mesh.positions.as_slice();

        for i in 0..vertex_count {
            let x = positions[3 * i + 0];
            let y = positions[3 * i + 1];
            let z = positions[3 * i + 2];

            let vertex = Vert {
                pos: glam::vec3(x, y, z),
                color: glam::vec3(0.8, 0.8, 0.8)
            };

            vertices.push(vertex);
        }

        let indices = mesh.indices.clone();

        let vert_buffer = plate::VertexBuffer::new(device, &vertices, cmd_pool)?;
        let index_buffer = plate::IndexBuffer::new(device, &indices, cmd_pool)?;

        Ok(Self {
            vert_buffer,
            index_buffer,
            index_count: indices.len() as u32,
        })
    }
}

impl<V> Renderable for Model<V> {
    type Vert = V;

    fn vert_buffer(&self) -> &plate::VertexBuffer<Self::Vert> {
        &self.vert_buffer
    }
    fn index_buffer(&self) -> &plate::IndexBuffer<u32> {
        &self.index_buffer
    }
    fn index_count(&self) -> u32 {
        self.index_count
    }
}

