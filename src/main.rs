use winit::event::VirtualKeyCode;

use plate_game::*;

struct Object<V> {
    model: Model<V>,
    transform: Transform,
}

impl<V> Renderable for Object<V> {
    type Vert = V;

    fn vert_buffer(&self) -> &plate::VertexBuffer<Self::Vert> {
        &self.model.vert_buffer()
    }
    fn index_buffer(&self) -> &plate::IndexBuffer<u32> {
        &self.model.index_buffer()
    }
    fn index_count(&self) -> u32 {
        self.model.index_count()
    }
}

impl<V> HasTransform for Object<V> {
    fn transform(&self) -> &Transform {
        &self.transform
    }
    fn transform_mut(&mut self) -> &mut Transform {
        &mut self.transform
    }
}

struct Cam {
    transform: Transform,
}

impl HasTransform for Cam {
    fn transform(&self) -> &Transform {
        &self.transform
    }
    fn transform_mut(&mut self) -> &mut Transform {
        &mut self.transform
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let event_loop = winit::event_loop::EventLoop::new();
    let window = winit::window::WindowBuilder::new().build(&event_loop)?;
    let mut renderer = Renderer::new(&window)?;

    let map = Object {
        model: Model::new(&renderer.device, &renderer.cmd_pool)?,
        transform: Transform::IDENTITY,
    };

    let mut cam = Cam {
        transform: Transform::IDENTITY,
    };

    let mut input = winit_input_helper::WinitInputHelper::new();

    event_loop.run(move |event, _, control_flow| {
        *control_flow = winit::event_loop::ControlFlow::Poll;

        if input.update(&event) {
            if input.key_held(VirtualKeyCode::Right) {
                cam.transform.rot = (glam::Quat::from_rotation_y(0.02) * cam.transform.rot).normalize()
            } else if input.key_held(VirtualKeyCode::Left) {
                cam.transform.rot = (glam::Quat::from_rotation_y(-0.02) * cam.transform.rot).normalize()
            }
            if input.key_held(VirtualKeyCode::Up) {
                cam.transform.rot = (cam.transform.rot * glam::Quat::from_rotation_x(0.02)).normalize()
            } else if input.key_held(VirtualKeyCode::Down) {
                cam.transform.rot = (cam.transform.rot * glam::Quat::from_rotation_x(-0.02)).normalize()
            }

            let x = if input.key_held(VirtualKeyCode::D) {
                0.1
            } else if input.key_held(VirtualKeyCode::A) {
                -0.1
            } else { 0.0 };
            let z = if input.key_held(VirtualKeyCode::W) {
                0.1
            } else if input.key_held(VirtualKeyCode::S) {
                -0.1
            } else { 0.0 };
            let y = if input.key_held(VirtualKeyCode::Q) {
                0.1
            } else if input.key_held(VirtualKeyCode::E) {
                -0.1
            } else { 0.0 };

            cam.translate(cam.quat() * glam::vec3(x, y, z));
        }

        match event {
            winit::event::Event::WindowEvent { event, window_id } if window_id == window.id() => {
                match event {
                    winit::event::WindowEvent::CloseRequested => *control_flow = winit::event_loop::ControlFlow::Exit,
                    _ => (),
                }
            }
            winit::event::Event::MainEventsCleared => window.request_redraw(),
            winit::event::Event::RedrawRequested(window_id) if window_id == window.id() => {
                renderer.render(&map, &cam);
            }
            winit::event::Event::LoopDestroyed => renderer.exit().unwrap(),
            _ => (),
        }
    })
}
