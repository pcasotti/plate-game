#version 450

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 color;

layout(set = 0, binding = 0) uniform Cam {
    mat4 proj;
    mat4 view;
} cam;

layout(set = 0, binding = 1) uniform Model {
    mat4 model;
} obj;

layout(location = 0) out vec3 fragColor;

void main() {
    gl_Position = cam.proj * cam.view * obj.model * vec4(pos, 1.0);
    fragColor = color;
}
