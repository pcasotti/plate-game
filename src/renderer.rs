use std::sync::Arc;

use plate::{plate_macros, VertexDescription};

use crate::HasTransform;

pub trait Renderable {
    type Vert;

    fn vert_buffer(&self) -> &plate::VertexBuffer<Self::Vert>;
    fn index_buffer(&self) -> &plate::IndexBuffer<u32>;
    fn index_count(&self) -> u32;
}

pub trait Camera {
    fn proj(&self, aspect_ration: f32) -> glam::Mat4;
    fn view(&self) -> glam::Mat4;
}

#[repr(C)]
#[derive(plate_macros::Vertex)]
pub struct Vert {
    #[vertex(loc = 0, format = "R32G32B32_SFLOAT")]
    pub pos: glam::Vec3,
    #[vertex(loc = 1, format = "R32G32B32_SFLOAT")]
    pub color: glam::Vec3,
}

#[repr(C)]
struct CamUbo {
    proj: glam::Mat4,
    view: glam::Mat4,
}

#[repr(C)]
struct Ubo {
    model: glam::Mat4,
}

pub struct Renderer {
    pub device: Arc<plate::Device>,
    swapchain: plate::Swapchain,
    pipeline: plate::Pipeline,

    #[allow(dead_code)]
    pub cmd_pool: plate::CommandPool,
    cmd_buffer: plate::CommandBuffer,

    #[allow(dead_code)]
    descriptor_pool: plate::DescriptorPool,
    descriptor_set: plate::DescriptorSet,

    cam_buffer: plate::MappedBuffer<CamUbo>,
    model_buffer: plate::MappedBuffer<Ubo>,

    fence: plate::Fence,
    acquire_sem: plate::Semaphore,
    present_sem: plate::Semaphore,
}

impl Renderer {
    pub fn new(window: &winit::window::Window) -> Result<Self, plate::Error> {
        let instance = plate::Instance::new(Some(window), &Default::default())?;
        let surface = plate::Surface::new(&instance, window)?;
        let device = plate::Device::new(instance, surface, &Default::default())?;
        let swapchain = plate::Swapchain::new(&device, &window)?;

        let set_layout = plate::DescriptorSetLayout::new(
            &device,
            &[
                plate::LayoutBinding {
                    binding: 0,
                    count: 1,
                    stage: plate::ShaderStage::VERTEX,
                    ty: plate::DescriptorType::UNIFORM_BUFFER,
                },
                plate::LayoutBinding {
                    binding: 1,
                    count: 1,
                    stage: plate::ShaderStage::VERTEX,
                    ty: plate::DescriptorType::UNIFORM_BUFFER,
                },
            ]
        )?;

        let pipeline = plate::Pipeline::new(
            &device,
            swapchain.render_pass(),
            vk_shader_macros::include_glsl!("src/shaders/shader.vert"),
            vk_shader_macros::include_glsl!("src/shaders/shader.frag"),
            &plate::PipelineParameters {
                vertex_binding_descriptions: Vert::binding_descriptions(),
                vertex_attribute_descriptions: Vert::attribute_descriptions(),
                descriptor_set_layouts: &[&set_layout],
                ..Default::default()
            }
        )?;

        let cmd_pool = plate::CommandPool::new(&device)?;
        let cmd_buffer = cmd_pool.alloc_cmd_buffer(plate::CommandBufferLevel::PRIMARY)?;

        let cam_buffer: plate::Buffer<CamUbo> = plate::Buffer::new(
            &device,
            1,
            plate::BufferUsageFlags::UNIFORM_BUFFER,
            plate::SharingMode::EXCLUSIVE,
            plate::MemoryPropertyFlags::HOST_VISIBLE | plate::MemoryPropertyFlags::HOST_COHERENT,
        )?;
        let model_buffer: plate::Buffer<Ubo> = plate::Buffer::new(
            &device,
            1,
            plate::BufferUsageFlags::UNIFORM_BUFFER,
            plate::SharingMode::EXCLUSIVE,
            plate::MemoryPropertyFlags::HOST_VISIBLE | plate::MemoryPropertyFlags::HOST_COHERENT,
        )?;

        let descriptor_pool = plate::DescriptorPool::builder()
            .add_size(plate::DescriptorType::UNIFORM_BUFFER, 2)
            .build(&device)?;
        let descriptor_set = plate::DescriptorAllocator::new(&device)
            .add_buffer_binding(0, plate::DescriptorType::UNIFORM_BUFFER, &cam_buffer)
            .add_buffer_binding(1, plate::DescriptorType::UNIFORM_BUFFER, &model_buffer)
            .allocate(&set_layout, &descriptor_pool)?;

        let model_buffer = model_buffer.map()?;
        let cam_buffer = cam_buffer.map()?;

        let fence = plate::Fence::new(&device, plate::FenceFlags::SIGNALED)?;
        let acquire_sem = plate::Semaphore::new(&device, plate::SemaphoreFlags::empty())?;
        let present_sem = plate::Semaphore::new(&device, plate::SemaphoreFlags::empty())?;

        Ok(Self {
            device,
            swapchain,
            pipeline,

            cmd_pool,
            cmd_buffer,

            descriptor_pool,
            descriptor_set,

            cam_buffer,
            model_buffer,

            fence,
            acquire_sem,
            present_sem,
        })
    }

    pub fn render<O: Renderable + HasTransform, C: Camera>(&mut self, obj: &O, cam: &C) {
        self.fence.wait().unwrap();
        self.fence.reset().unwrap();

        let (i, _) = self.swapchain.next_image(&self.acquire_sem).unwrap();

        self.model_buffer.write(&[Ubo { model: obj.mat4() }]);
        self.cam_buffer.write(&[CamUbo { proj: cam.proj(self.swapchain.aspect_ratio()), view: cam.view() }]);

        self.cmd_buffer.record(plate::CommandBufferUsageFlags::empty(), || {
            self.swapchain.begin_render_pass(&self.cmd_buffer, i.try_into().unwrap());

            self.pipeline.bind(&self.cmd_buffer, self.swapchain.extent());
            obj.vert_buffer().bind(&self.cmd_buffer);
            obj.index_buffer().bind(&self.cmd_buffer);
            self.descriptor_set.bind(&self.cmd_buffer, &self.pipeline, 0, &[]).unwrap();
            self.cmd_buffer.draw_indexed(obj.index_count(), 1, 0, 0, 0);

            self.swapchain.end_render_pass(&self.cmd_buffer);
        }).unwrap();

        self.device.queue_submit(
            self.device.graphics_queue,
            &self.cmd_buffer,
            plate::PipelineStage::COLOR_ATTACHMENT_OUTPUT,
            Some(&self.acquire_sem),
            Some(&self.present_sem),
            Some(&self.fence),
        ).unwrap();

        self.swapchain.present(i, &self.present_sem).unwrap();
    }

    pub fn exit(&self) -> Result<(), plate::Error> {
        self.device.wait_idle()
    }
}
